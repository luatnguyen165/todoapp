(async function () {
  let str = '';
  axios.get('/todo/list').then((res) => {
    res.data.data.map((item) => {
      str += '<div class="todo-list">';
      str += `<div class="date"> ${new Date(
        item.createdAt,
      ).toISOString()}</div>`;
      str += '<div class="row">';
      str += '<div class="todos">';
      // str += `<input type="checkbox" onchange="doalert(this.id)" class="todoItem" id=${item._id}  value="I will buy a bike"/>`;
      str += `<label for="1" class="todoItem" ${
        item.completed ? "style = 'text-decoration: line-through'" : ''
      }  >${item.text} </label>`;
      str += `<i class="fa-regular fa-pen-to-square" onclick ="reply(this.id)"  id=${item._id} ></i>`;
      str += `<i class="fa-solid fa-xmark" id=${item._id} onclick ="reply_click(this.id)"  ></i>`;
      str += '</div>';
      str += `<div class="circle ${item.priority}"></div>`;
      str += '</div>';
      str += '</div>';
      str += '<div class="box" id="box">';
      // str += '<i class="fa-solid fa-xmark" id="close" onclick="close(this)"></i>';
      str += '<h2 class="title">Edit</h2>';
      str += '<input type="text" class="editItem" id="myText" />';
      str += '<select name="priority" class="priority" id="priorityd">';
      str += '<option selected disabled>Priority</option>';
      str += '<option value="high">High</option>';
      str += '<option value="middle">Middle</option>';
      str += '<option value="low">Low</option>';
      str += '</select>';
      str += '<select name="complete" class="priority" id="complete">';
      str += '<option value="true">Done</option>';
      str += '<option value="false">Pending</option>';
      str += '</select>';
      str += `<input type="submit" class="editSubmit" value="Save" id=${item._id} onclick="editSubmit(this.id)" />`;
      str += '</div>';
      document.getElementById('listLeft').innerHTML = str;
    });
  });
})();
function reply_click(id) {
  axios.delete('todo/' + id).then((res) => {
    window.location.reload();
  });
}
function onAdd() {
  let todo = document.getElementById('todo-input').value;
  var e = document.getElementById('priority');
  var date = document.getElementById('date-input').value;
  axios
    .post('todo', {
      text: todo,
      priority: e.value,
      createdAt: date ? date : new Date(),
    })
    .then((res) => {
      window.location.reload();
    });
}
async function doalert(id) {
  if (this.checked) {
    alert('checked', id);
  } else {
    alert('unchecked' + id);
  }
}
async function reply(id) {
  var element = document.getElementById('box');
  element.classList.add('active');
  let data = await axios.get('/todo/getTodo/' + id);
  document.getElementById('myText').value = data.data.data.text;
  document.getElementById('priorityd').value = data.data.data.priority;
}
function editSubmit(id) {
  let text = document.getElementById('myText').value;
  var e = document.getElementById('priorityd').value;
  let complete = document.getElementById('complete').value;
  // alert(JSON.stringify({text,e,complete}))
  axios
    .put('todo/' + id, {
      text,
      priority: e,
      completed: complete,
    })
    .then(() => {
      window.location.reload();
    });
  var element = document.getElementById('box');

  element.classList.remove('active');
}
function searchTodo() {
  let search = document.getElementById('text-todo-input').value;
  let date = document.getElementById('date-input-search').value;
  // alert(search);
  if (date === '') {
    console.log('vào đây........');
    let str = '';
    axios.get(`/todo/list?name=${search}`).then((res) => {
      res.data.data.map((item) => {
        console.log('item', item);
        str += '<div class="todo-list">';
        str += `<div class="date"> ${new Date(
          item.createdAt,
        ).toISOString()}</div>`;
        str += '<div class="row">';
        str += '<div class="todos">';
        // str += `<input type="checkbox" onchange="doalert(this.id)" class="todoItem" id=${item._id}  value="I will buy a bike"/>`;
        str += `<label for="1" class="todoItem" ${
          item.completed ? "style = 'text-decoration: line-through'" : ''
        }  >${item.text} </label>`;
        str += `<i class="fa-regular fa-pen-to-square" onclick ="reply(this.id)"  id=${item._id} ></i>`;
        str += `<i class="fa-solid fa-xmark" id=${item._id} onclick ="reply_click(this.id)"  ></i>`;
        str += '</div>';
        str += `<div class="circle ${item.priority}"></div>`;
        str += '</div>';
        str += '</div>';
        str += '<div class="box" id="box">';
        // str += '<i class="fa-solid fa-xmark" id="close" onclick="close(this)"></i>';
        str += '<h2 class="title">Edit</h2>';
        str += '<input type="text" class="editItem" id="myText" />';
        str += '<select name="priority" class="priority" id="priorityd">';
        str += '<option selected disabled>Priority</option>';
        str += '<option value="high">High</option>';
        str += '<option value="middle">Middle</option>';
        str += '<option value="low">Low</option>';
        str += '</select>';
        str += '<select name="complete" class="priority" id="complete">';
        str += '<option value="true">Done</option>';
        str += '<option value="false">Pending</option>';
        str += '</select>';
        str += `<input type="submit" class="editSubmit" value="Save" id=${item._id} onclick="editSubmit(this.id)" />`;
        str += '</div>';
        document.getElementById('searchRight').innerHTML = str;
      });
    });
  } else if (search == '') {
    let str = '';
    axios.get(`/todo/list?from=${new Date(date).toISOString()}`).then((res) => {
      res.data.data.map((item) => {
        str += '<div class="todo-list">';
        str += `<div class="date"> ${new Date(
          item.createdAt,
        ).toISOString()}</div>`;
        str += '<div class="row">';
        str += '<div class="todos">';
        // str += `<input type="checkbox" onchange="doalert(this.id)" class="todoItem" id=${item._id}  value="I will buy a bike"/>`;
        str += `<label for="1" class="todoItem" ${
          item.completed ? "style = 'text-decoration: line-through'" : ''
        }  >${item.text} </label>`;
        str += `<i class="fa-regular fa-pen-to-square" onclick ="reply(this.id)"  id=${item._id} ></i>`;
        str += `<i class="fa-solid fa-xmark" id=${item._id} onclick ="reply_click(this.id)"  ></i>`;
        str += '</div>';
        str += `<div class="circle ${item.priority}"></div>`;
        str += '</div>';
        str += '</div>';
        str += '<div class="box" id="box">';
        // str += '<i class="fa-solid fa-xmark" id="close" onclick="close(this)"></i>';
        str += '<h2 class="title">Edit</h2>';
        str += '<input type="text" class="editItem" id="myText" />';
        str += '<select name="priority" class="priority" id="priorityd">';
        str += '<option selected disabled>Priority</option>';
        str += '<option value="high">High</option>';
        str += '<option value="middle">Middle</option>';
        str += '<option value="low">Low</option>';
        str += '</select>';
        str += '<select name="complete" class="priority" id="complete">';
        str += '<option value="true">Done</option>';
        str += '<option value="false">Pending</option>';
        str += '</select>';
        str += `<input type="submit" class="editSubmit" value="Save" id=${item._id} onclick="editSubmit(this.id)" />`;
        str += '</div>';
        document.getElementById('searchRight').innerHTML = str;
      });
    });
  } else {
    let str = '';
    axios.get(`/todo/list?name=${search}&& from= ${date}`).then((res) => {
      res.data.data.map((item) => {
        str += '<div class="todo-list">';
        str += `<div class="date"> ${new Date(
          item.createdAt,
        ).toISOString()}</div>`;
        str += '<div class="row">';
        str += '<div class="todos">';
        // str += `<input type="checkbox" onchange="doalert(this.id)" class="todoItem" id=${item._id}  value="I will buy a bike"/>`;
        str += `<label for="1" class="todoItem" ${
          item.completed ? "style = 'text-decoration: line-through'" : ''
        }  >${item.text} </label>`;
        str += `<i class="fa-regular fa-pen-to-square" onclick ="reply(this.id)"  id=${item._id} ></i>`;
        str += `<i class="fa-solid fa-xmark" id=${item._id} onclick ="reply_click(this.id)"  ></i>`;
        str += '</div>';
        str += `<div class="circle ${item.priority}"></div>`;
        str += '</div>';
        str += '</div>';
        str += '<div class="box" id="box">';
        // str += '<i class="fa-solid fa-xmark" id="close" onclick="close(this)"></i>';
        str += '<h2 class="title">Edit</h2>';
        str += '<input type="text" class="editItem" id="myText" />';
        str += '<select name="priority" class="priority" id="priorityd">';
        str += '<option selected disabled>Priority</option>';
        str += '<option value="high">High</option>';
        str += '<option value="middle">Middle</option>';
        str += '<option value="low">Low</option>';
        str += '</select>';
        str += '<select name="complete" class="priority" id="complete">';
        str += '<option value="true">Done</option>';
        str += '<option value="false">Pending</option>';
        str += '</select>';
        str += `<input type="submit" class="editSubmit" value="Save" id=${item._id} onclick="editSubmit(this.id)" />`;
        str += '</div>';
        document.getElementById('searchRight').innerHTML = str;
      });
    });
  }
}

function logout() {
  window.location.href = '/auth/sign-in';
  var allCookies = document.cookie.split(';');
  for (var i = 0; i < allCookies.length; i++)
    document.cookie = allCookies[i] + '=;expires=' + new Date(0).toUTCString();

  displayCookies.innerHTML = document.cookie;

}
