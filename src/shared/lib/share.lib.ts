export class ShareLib {
  public _toFirstUpperCase(payload) {
    const str = payload.split(' ');
    for (let i = 0; i < str.length; i++) {
      str[i] = str[i].slice(0, 1).toUpperCase() + str[i].slice(1).toLowerCase();
    }
    return str.join(' ');
  }
  public randome(length) {
    return Math.floor(
      Math.pow(10, length - 1) +
        Math.random() * (Math.pow(10, length) - Math.pow(10, length - 1) - 1),
    );
  }
}
