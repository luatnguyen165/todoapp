import { Injectable } from '@nestjs/common';
import { MailerService } from '@nestjs-modules/mailer';
import { MailDto } from './dto/mail.dto';
@Injectable()
export class MailService {
  constructor(private mailerService: MailerService) {}
  async sendEmail(payload: MailDto) {
    try {
      const { to, subject, temp, context } = payload;
      const send = await this.mailerService.sendMail({
        to,
        subject,
        template: temp,
        context,
      });
      
      return send.messageId;
    } catch (error) {
      console.log(error);
    }
  }
}
