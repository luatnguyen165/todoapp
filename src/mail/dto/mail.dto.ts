import { IsString, IsEmail, IsOptional } from 'class-validator';
export class MailDto {

  @IsString()
  @IsEmail()
  to: string;

  @IsString()
  subject: string;

  @IsString()
  temp: string;

  @IsOptional()
  context: object;
}
