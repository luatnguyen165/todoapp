import { IsString, IsEmail, Max, Min, IsInt } from 'class-validator';

export class RegisterUserDto {
  @IsString()
  @Max(50)
  @Min(5)
  fullName: string;

  @IsString()
  @IsEmail()
  email: string;

  @IsString()
  @Min(8)
  password: string;

  @IsString()
  phone: string;

}
export class LoginDto {
  @IsString()
  @IsEmail()
  email: string;

  @IsString()
  @Min(8)
  password: string;
}

export class ForgotPasswordEmailDto {
  @IsString()
  @IsEmail()
  email: string;

}
export class ForgotPasswordCodeDto {
  @IsInt()
  code: number;

  @IsString()
  re_password: string;
}
export class ResendEmailDto {
  @IsEmail()
  email: string;
}
