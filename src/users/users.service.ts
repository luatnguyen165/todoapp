import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import {
  LoginDto,
  RegisterUserDto,
  ForgotPasswordEmailDto,
  ForgotPasswordCodeDto,
  ResendEmailDto,
} from './dto/create-user.dto';
import {
  ForgotPasswordInterface,
  LoginUserInterface,
  RegisterUserInterface,
} from './interfaces/user.interface';
import { User, UserDocument } from './schema/user.schema';
import * as bcrypt from 'bcrypt';
import * as _ from 'lodash';
// import { RedisCacheService } from 'src/redis/redis.service';
import { USER_CODE, USER_MESSAGE } from './constants/user.constant';
import { CACHE_MANAGER, Inject, Injectable } from '@nestjs/common';
import { Cache } from 'cache-manager';
import { MailService } from 'src/mail/mail.service';
import { ShareLib } from 'src/shared/lib/share.lib';
import { COMMON_MESSAGE } from 'src/shared/constants/share.constant';

@Injectable()
export class UsersService {
  constructor(
    @InjectModel(User.name) private userModel: Model<UserDocument>,
    @Inject(CACHE_MANAGER) private cacheManager: Cache, // private readonly redisModel: RedisCacheService,
    private readonly mail: MailService,
  ) {}
  private lib = new ShareLib();
  // register account and send email for users
  async registerAccount(
    createUserDto: RegisterUserDto,
  ): Promise<RegisterUserInterface> {
    try {
      const { email } = createUserDto;
      const userExist = await this.userModel.findOne({ email });
      if (userExist) {
        return {
          code: USER_CODE.EMAIL_EXIST,
          message: this.lib._toFirstUpperCase(USER_MESSAGE.EMAIL_EXIST),
        };
      }
      createUserDto.password = await this._hash(createUserDto.password);
      const user = new this.userModel(createUserDto);
      const userSave = await user.save();
      if (!userSave) {
        return {
          code: USER_CODE.REGISTER_FAILED,
          message: this.lib._toFirstUpperCase(USER_MESSAGE.REGISTER_FAILED),
        };
      }
      // send email cho no de no done
      const rand = this.lib.randome(6);
      await this.cacheManager.set(
        `${COMMON_MESSAGE.ACTIVE_CODE}${rand}`,
        email,
        24 * 60 * 60 * 1000,
      );

      const payload = {
        to: email,
        subject: 'Activation Account',
        temp: './active.hbs',
        context: {
          url: `${process.env.EMAIL_URL}/auth/active?code=${rand}`,
          code: rand,
        },
      };
      await this.mail.sendEmail(payload);
      return {
        code: USER_CODE.REGISTER_SUCCESS,
        message: this.lib._toFirstUpperCase(USER_MESSAGE.REGISTER_SUCCESS),
      };
    } catch (error) {
      throw new Error(error);
    }
  }
  // User active account query url http://localhost:3000?code=879281&&email=user@example.com
  async isActiveAccount(payload) {
    try {
      const { code } = payload;
      const codeAccount = await this.cacheManager.get(
        `${COMMON_MESSAGE.ACTIVE_CODE}${code}`,
      );
      console.log('payload', payload);
      const emailIsActive = await this.userModel.findOne({
        $and: [{ email: codeAccount }, { isActive: true }],
      });
      if (emailIsActive) {
        return {
          code: USER_CODE.PREVIOUS_ACTIVE_ACCOUNT,
          message: this.lib._toFirstUpperCase(
            USER_MESSAGE.PREVIOUS_ACTIVE_ACCOUNT,
          ),
        };
      }
      if (!codeAccount) {
        return {
          code: USER_CODE.INVALID_CODE,
          message: this.lib._toFirstUpperCase(USER_MESSAGE.INVALID_CODE),
        };
      }
      const update = await this.userModel.findOneAndUpdate(
        {
          email: codeAccount,
        },
        {
          isActive: true,
        },
      );
      if (!update) {
        return {
          code: USER_CODE.ACTIVED_ACCOUNT_FAILED,
          message: this.lib._toFirstUpperCase(
            USER_MESSAGE.ACTIVED_ACCOUNT_FAILED,
          ),
        };
      }

      return {
        code: USER_CODE.ACTIVED_ACCOUNT,
        message: this.lib._toFirstUpperCase(USER_MESSAGE.ACTIVED_ACCOUNT),
      };
    } catch (error) {
      throw new Error(error);
    }
  }

  // resend active account again
  async resendActiveAccount({ email }: ResendEmailDto) {
    try {
      const checkEmailActive = await this.userModel.findOne({ email });
      if (_.get(checkEmailActive, 'isActive', false) === true) {
        return {
          code: USER_CODE.PREVIOUS_ACTIVE_ACCOUNT,
          message: this.lib._toFirstUpperCase(
            USER_MESSAGE.PREVIOUS_ACTIVE_ACCOUNT,
          ),
        };
      }
      if (!checkEmailActive) {
        return {
          code: USER_CODE.EMAIL_IS_NOT_EXIST,
          message: this.lib._toFirstUpperCase(USER_MESSAGE.EMAIL_IS_NOT_EXIST),
        };
      }
      // send email active
      const randomeCode = this.lib.randome(6);
      await this.cacheManager.set(
        `${COMMON_MESSAGE.ACTIVE_CODE}${randomeCode}`,
        email,
        24 * 60 * 60 * 1000,
      );
      const payload = {
        to: email,
        subject: 'Activation Account',
        temp: './resend.hbs',
        context: {
          url: `${process.env.EMAIL_URL}/auth/active?code=${randomeCode}`,
          code: randomeCode,
        },
      };
      await this.mail.sendEmail(payload);
      return {
        code: USER_CODE.ACTIVED_ACCOUNT,
        message: this.lib._toFirstUpperCase(USER_MESSAGE.ACTIVED_ACCOUNT),
      };
    } catch (error) {
      throw new Error(error);
    }
  }

  async loginAccount(login: LoginDto): Promise<LoginUserInterface> {
    try {
      const { password, email } = login;
      const user = await this.userModel.findOne({ email });
      if (_.get(user, 'isActive', false) === false) {
        return {
          code: USER_CODE.ACCOUNT_NOT_ACTIVE,
          message: this.lib._toFirstUpperCase(USER_MESSAGE.ACCOUNT_NOT_ACTIVE),
        };
      }
      if (!user) {
        return {
          code: USER_CODE.LOGIN_FAILED,
          message: this.lib._toFirstUpperCase(USER_MESSAGE.LOGIN_FAILED),
        };
      }
      const hash = _.get(user, 'password', '');
      const isMatch = await this._compare(password, hash);
      if (!isMatch) {
        return {
          code: USER_CODE.LOGIN_FAILED,
          message: this.lib._toFirstUpperCase(USER_MESSAGE.LOGIN_FAILED),
        };
      }
      return {
        code: USER_CODE.LOGIN_SUCCESS,
        message: this.lib._toFirstUpperCase(USER_MESSAGE.LOGIN_SUCCESS),
        data: user,
      };
    } catch (error) {
      throw new Error(error);
    }
  }
  async forgotSendEmail(
    forgotPasswordEmailDto: ForgotPasswordEmailDto,
  ): Promise<ForgotPasswordInterface> {
    try {
      const { email } = forgotPasswordEmailDto;
      const findUser = await this.userModel.findOne({ email });

      if (!findUser) {
        return {
          code: USER_CODE.EMAIL_IS_NOT_EXIST,
          message: this.lib._toFirstUpperCase(USER_MESSAGE.EMAIL_IS_NOT_EXIST),
        };
      }
      const randomeCode = this.lib.randome(6);
      await this.cacheManager.set(
        `${COMMON_MESSAGE.RESET_CODE}${randomeCode}`,
        email,
        24 * 60 * 60 * 1000,
      );
      // send email for the user
      const payload = {
        to: email,
        subject: 'Activation Account',
        temp: './forgot.hbs',
        context: {
          url: `${process.env.EMAIL_URL}/auth/reset-password`,
          code: randomeCode,
        },
      };
      await this.mail.sendEmail(payload);
      return {
        code: USER_CODE.RESEND_EMAIL_ACTIVE,
        message: this.lib._toFirstUpperCase(USER_MESSAGE.RESEND_EMAIL_ACTIVE),
      };
    } catch (error) {
      throw new Error(error);
    }
  }
  async resetPassword(Forgot: ForgotPasswordCodeDto) {
    try {
      const { code, re_password } = Forgot;
      const codeCache = await this.cacheManager.get(
        `${COMMON_MESSAGE.RESET_CODE}${code}`,
      );
      if (!codeCache) {
        return {
          code: USER_CODE.INVALID_CODE,
          message: this.lib._toFirstUpperCase(USER_MESSAGE.INVALID_CODE),
        };
      }
      const update = await this.userModel.findOneAndUpdate(
        { email: codeCache },
        {
          password: await this._hash(re_password),
        },
      );
      if (!update) {
        return {
          code: USER_CODE.REGISTER_FAILED,
          message: this.lib._toFirstUpperCase(USER_MESSAGE.REGISTER_FAILED),
        };
      }
      return {
        code: USER_CODE.REGISTER_SUCCESS,
        message: this.lib._toFirstUpperCase(USER_MESSAGE.REGISTER_SUCCESS),
      };
    } catch (error) {
      throw new Error(error);
    }
  }

  private async _hash(password) {
    const salt = await bcrypt.genSalt();
    return await bcrypt.hash(password, salt);
  }
  private async _compare(password, hash) {
    return await bcrypt.compare(password, hash);
  }
}
