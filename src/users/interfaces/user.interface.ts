export interface RegisterUserInterface {
  message: string;
  code: number;
  data?: object;
}
export interface LoginUserInterface {
  message: string;
  code: number;
  data?: object;
}
export interface ForgotPasswordInterface {
  message: string;
  code: number;
}
