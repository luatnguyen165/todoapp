import { Module } from '@nestjs/common';
import { UsersService } from './users.service';
import { MongooseModule } from '@nestjs/mongoose';
import { User, UserSchema } from './schema/user.schema';
import { CacheModule } from '@nestjs/cache-manager';
import { MailModule } from 'src/mail/mail.module';
@Module({
  imports: [
    MongooseModule.forFeature([{ name: User.name, schema: UserSchema }]),
    CacheModule.register(),
    MailModule
  ],
  controllers: [],
  providers: [UsersService],
  exports: [UsersService]
})
export class UsersModule {}
