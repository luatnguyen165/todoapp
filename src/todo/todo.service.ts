import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import * as _ from 'lodash';
import { Model } from 'mongoose';
import { ShareLib } from 'src/shared/lib/share.lib';
import { TODO_CODE, TODO_MESSAGE } from './constants/todo.constant';
import { CreateTodoDto } from './dto/create-todo.dto';
import { UpdateTodoDto } from './dto/update-todo.dto';
import {
  todoListResponseInterface,
  todoResponseInterface,
} from './interfaces/todo.interface';
import { Todo, TodoDocument } from './schema/todo.schema';

@Injectable()
export class TodoService {
  constructor(@InjectModel(Todo.name) private todoModel: Model<TodoDocument>) {}
  private lib = new ShareLib();
  async create(createTodoDto: CreateTodoDto): Promise<todoResponseInterface> {
    try {
      const todo = new this.todoModel(createTodoDto);
      const todoSave = await todo.save();
      if (!todoSave) {
        return {
          code: TODO_CODE.CREATED_TODO_FAILURE,
          message: this.lib._toFirstUpperCase(
            TODO_MESSAGE.CREATED_TODO_FAILURE,
          ),
        };
      }
      return {
        code: TODO_CODE.CREATED_TODO_SUCCESS,
        message: this.lib._toFirstUpperCase(TODO_MESSAGE.CREATED_TODO_SUCCESS),
      };
    } catch (error) {
      throw new Error(error);
    }
  }

  async findAll({ name, from }): Promise<todoListResponseInterface> {
    try {
      const queryObj = {};
      if (!_.isEmpty(name)) {
        _.set(queryObj, 'text', {
          $regex: `${name}`,
          $options: 'i',
        });
      }
      if (!_.isEmpty(from)) {
        _.set(queryObj, 'createdAt', { $gte: from });
      }
      const listTodo = await this.todoModel.find(queryObj);
      return {
        code: TODO_CODE.GET_TODO_SUCCESS,
        message: this.lib._toFirstUpperCase(TODO_MESSAGE.GET_TODO_SUCCESS),
        data: listTodo,
      };
    } catch (error) {
      throw new Error(error);
    }
  }

  async findOne(id: string): Promise<todoResponseInterface> {
    try {
      const todo = await this.todoModel.findOne({
        _id: id,
      });
      if (!todo) {
        if (!todo) {
          return {
            code: TODO_CODE.GET_TODO_FAILURE,
            message: this.lib._toFirstUpperCase(TODO_MESSAGE.GET_TODO_FAILURE),
          };
        }
      }
      return {
        code: TODO_CODE.GET_TODO_SUCCESS,
        message: this.lib._toFirstUpperCase(TODO_MESSAGE.GET_TODO_SUCCESS),
        data: todo,
      };
    } catch (error) {}
  }

  async update(id: string, updateTodoDto: UpdateTodoDto) {
    try {
      const todo = await this.todoModel.findById({
        _id: id,
      });
      if (!todo) {
        return {
          code: TODO_CODE.GET_TODO_FAILURE,
          message: this.lib._toFirstUpperCase(TODO_MESSAGE.GET_TODO_FAILURE),
        };
      }
      const updateTodo = await this.todoModel.updateOne(
        { _id: todo.id },
        updateTodoDto,
      );
      if (!updateTodo || updateTodo.modifiedCount === 0) {
        return {
          code: TODO_CODE.UPDATE_TODO_FAILURE,
          message: this.lib._toFirstUpperCase(TODO_MESSAGE.UPDATE_TODO_FAILURE),
        };
      }
      return {
        code: TODO_CODE.UPDATE_TODO_SUCCESS,
        message: this.lib._toFirstUpperCase(TODO_MESSAGE.UPDATE_TODO_SUCCESS),
      };
    } catch (error) {
      throw new Error(error);
    }
  }

  async remove(id: string) {
    try {
      const todo = await this.todoModel.findById({
        _id: id,
      });
      if (!todo) {
        return {
          code: TODO_CODE.GET_TODO_FAILURE,
          message: this.lib._toFirstUpperCase(TODO_MESSAGE.GET_TODO_FAILURE),
        };
      }
      const deleteOne = await this.todoModel.deleteOne({ _id: id });
      if (!deleteOne || deleteOne.deletedCount !== 1) {
        return {
          code: TODO_CODE.DELETE_TODO_FAILURE,
          message: this.lib._toFirstUpperCase(TODO_MESSAGE.DELETE_TODO_FAILURE),
        };
      }
      return {
        code: TODO_CODE.DELETE_TODO_SUCCESS,
        message: this.lib._toFirstUpperCase(TODO_MESSAGE.DELETE_TODO_SUCCESS),
      };
    } catch (error) {
      throw new Error(error);
    }
  }
}
