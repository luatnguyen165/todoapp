import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Exclude, Transform } from 'class-transformer';
import { Document } from 'mongoose';

export type TodoDocument = Todo & Document;

@Schema({
  timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' },
})
export class Todo {
  @Transform(({ value }) => value.toString())
  _id: string;
  @Prop({
    required: true,
  })
  text: string;

  @Prop({
    default: false,
  })
  completed: boolean;
  @Prop()
  priority: string;

}

export const TodoSchema = SchemaFactory.createForClass(Todo);


