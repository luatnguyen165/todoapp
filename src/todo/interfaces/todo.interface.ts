export interface todoResponseInterface {
  code: number;
  message: string;
  data?: object;
}

export interface todoListResponseInterface {
  code: number;
  message: string;
  data?: Array<todoList>;
}

export interface todoList {
  _id: string;
  text: string;
  priority: string;
  completed: boolean;
}
