import { Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';
import { JwtService } from '@nestjs/jwt';
@Injectable()
export class todoMiddleware implements NestMiddleware {
  constructor(private jwtService: JwtService) {}
  async use(req: Request, res: Response, next: NextFunction) {
    if (req.cookies.Authorization) {
      const decode = await this.jwtService.verify(req.cookies.Authorization, {
        secret: process.env.SECRET_KEY_JWT,
      });
      const { exp } = decode;
      if (exp > (new Date().getTime() + 1) / 1000) {
        next();
      } else {
        res.redirect('/auth/sign-in');
      }
    } else {
      res.redirect('/auth/sign-in');
    }
  }
}
