import {
  Controller,
  Get,
  Post,
  Body,
  Query,
  Render,
  Res,
  ValidationPipe,
} from '@nestjs/common';
import { HttpExceptionFilter } from 'src/http-exception.filter';
import { AuthService } from './auth.service';
import {
  ForgotPasswordCodeDto,
  ForgotPasswordEmailDto,
  LoginDto,
  RegisterUserDto,
} from './dto/create-auth.dto';

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Get('sign-up')
  @Render('register.hbs')
  async signUp() {
    return {};
  }
  @Post('sign-up')
  async registerAccount(@Body() data: RegisterUserDto) {
    return await this.authService.registerAccount(data);
  }

  @Get('active')
  @Render('thanks.hbs')
  async activeNewAccount(@Query('code') code: string) {
    return await this.authService.activeNewAccount({ code });
  }
  @Get('sign-in')
  @Render('login.hbs')
  async login() {
    return {};
  }
  @Post('sign-in')
  async loginAccount(
    @Body() data: LoginDto,
    @Res({ passthrough: true }) response: Response,
  ) {
    return await this.authService.loginAccount(data, response);
  }
  @Get('forgot-password')
  @Render('forgot.hbs')
  async forgot() {
    return {};
  }

  @Post('forgot-password')
  async forgotAccount(@Body() data: ForgotPasswordEmailDto) {
    return await this.authService.forgotPassword(data);
  }

  @Get('reset-password')
  @Render('reset-password.hbs')
  async resetPass() {
    return {};
  }
  @Post('reset-password')
  async resetAccount(@Body() data: ForgotPasswordCodeDto) {
    return await this.authService.resetNewPassword(data);
  }

  @Render('resend')
  @Get('resend')
  async resend() {
    return {};
  }
  @Post('re-send-active')
  async resendActiveAccount(@Body() data: ForgotPasswordEmailDto) {
    return await this.authService.reSendEmailActive(data);
  }
}
