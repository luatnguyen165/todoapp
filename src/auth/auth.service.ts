import { Injectable } from '@nestjs/common';
import { UsersService } from './../users/users.service';
import {
  ForgotPasswordCodeDto,
  ForgotPasswordEmailDto,
  LoginDto,
  RegisterUserDto,
} from './dto/create-auth.dto';
import { JwtService } from '@nestjs/jwt';
import * as _ from 'lodash';
@Injectable()
export class AuthService {
  constructor(
    private readonly usersService: UsersService,
    private jwtService: JwtService,
  ) {}

  async registerAccount(data: RegisterUserDto) {
    try {
      const user = await this.usersService.registerAccount(data);
      if (_.get(user, 'code', 200) !== 200) {
        return {
          code: user.code,
          message: user.message,
        };
      }
      return {
        code: user.code,
        message: user.message,
      };
    } catch (error) {
      throw new Error(error);
    }
  }
  async loginAccount(login: LoginDto, response: any) {
    try {
      const user = await this.usersService.loginAccount(login);
      if (user.code === 200) {
        const payload = {
          email: _.get(user, 'data.email', ''),
          fullName: _.get(user, 'data.fullName', ''),
        };

        const access_token = await this.jwtService.signAsync(payload, {
          secret: process.env.SECRET_KEY_JWT,
          expiresIn: process.env.EXPIRES_JWT,
        });
        response.cookie('Authorization', access_token);
        return {
          code: user.code,
          message: user.message,
        };
      }
      return {
        code: user.code,
        message: user.message,
      };
    } catch (error) {
      throw new Error(error);
    }
  }
  async forgotPassword(data: ForgotPasswordEmailDto) {
    try {
      return await this.usersService.forgotSendEmail(data);
    } catch (error) {
      throw new Error(error);
    }
  }
  async resetNewPassword(data: ForgotPasswordCodeDto) {
    try {
      return await this.usersService.resetPassword(data);
    } catch (error) {
      throw new Error(error);
    }
  }
  async reSendEmailActive(data: ForgotPasswordEmailDto) {
    try {
      return await this.usersService.resendActiveAccount(data);
    } catch (error) {
      throw new Error(error);
    }
  }
  async activeNewAccount(data: any) {
    try {
      return await this.usersService.isActiveAccount(data);
    } catch (error) {
      throw new Error(error);
    }
  }
}
